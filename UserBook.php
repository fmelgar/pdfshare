<?php

class UserBook {

    private $objectId;
    private $BookId;
    private $UserId;


    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }

    public function getObjectId()
    {
        return $this->objectId;
    }

    public function setBookId($BookId)
    {
        $this->BookId = $BookId;
    }
   
    public function getBookId()
    {
        return $this->BookId;
    }

    public function setUserId($UserId)
    {
        $this->UserId = $UserId;
    }

   
    public function getUserId()
    {
        return $this->UserId;
    }
}
?>
