<?php

class UsersLogIn{

	private $bookName;
	private $bookURL;
	private $CategoryId;
	private $bookThumbnailUrl;
	

	public function getBookName()
	{
		return $this->bookName;
	}

	public function setBookName($bookName)
	{
		$this->bookName = $bookName; 
	}

	public function getBookUrl()
	{
		return $this->bookURL;
	}

	public function setBookUrl($bookURL)
	{
		$this->bookURL = $bookURL;
	}

	public function getCategoryId()
	{
		return $this->CategoryId;
	}

	public function setCategoryId($CategoryId)
	{
		$this->CategoryId = $CategoryId;
	}

	public function getBookThumbnailUrl()
	{
		return $this->bookThumbnailUrl;
	}

	public function setBookThumbnailUrl($bookThumbnailUrl)
	{
		$this->bookThumbnailUrl = $bookThumbnailUrl; 
	}

	

}

?>