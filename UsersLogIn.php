?php

class UsersLogIn{

	private $Status;
	private $Password;
	private $Name;
	private $LastLogIn;
	private $Email;
	private $objectId;


	public function getObjectId()
	{
		return $this->objectId;
	}

	public function setObjectId($objectId)
	{
		$this->objectId = $objectId;
	}

	public function getStatus()
	{
		return $this->Status;
	}

	public function setStatus($status)
	{
		$this->Status = $status; 
	}

	public function getPassword()
	{
		return $this->Password;
	}

	public function setPassword($password)
	{
		$this->Password = $password;
	}

	public function getName()
	{
		return $this->Name;
	}

	public function setName($name)
	{
		$this->Name = $name;
	}

	public function getLastLogIn()
	{
		return $this->LastLogIn;
	}

	public function setLastLogIn($lastLogin)
	{
		$this->LastLogIn = $lastLogin; 
	}

	public function getEmail()
	{
		return $this->Email;
	}

	public function setEmail($email)
	{
		$this->Email = $email;
	}

}

?>