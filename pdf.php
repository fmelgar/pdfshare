<?php

   session_start();
    
   if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
   } else {
      header('Location: index.php');   
    exit;
  }  
 
?>

<?php

use backendless\Backendless;
use backendless\services\persistence\BackendlessDataQuery;
include "./autoload.php";

Backendless::initApp('21880115-D6BD-1ECE-FF0D-234E6063AA00', '12BD10F1-D42A-0259-FF10-D53274AB6000', 'v1');

$id = $_GET['id'];

if ($id == "") {
	header('LOCATION: menu.php');
}else{

	$result = Backendless::$Persistence->of("Book")->findById( $id, $relation_depth= null );

	$file = $result['bookURL'];
	$filename = result['bookName'] . '.pdf';
	header('Content-type: application/pdf');	
	header('Content-Disposition: inline; filename="' . $filename . '"');
	header('Content-Transfer-Encoding: binary');
	header('Accept-Ranges: bytes');
	@readfile($file);
}

?>