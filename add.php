<?php

   session_start();
    
   if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    
   } else {
      header('Location: index.php');   
    exit;
  }  
 
?>

<?php

    use backendless\Backendless;
    use backendless\services\persistence\BackendlessDataQuery;
    include "./autoload.php";
    include "./UserBook.php";

    Backendless::initApp('21880115-D6BD-1ECE-FF0D-234E6063AA00', '12BD10F1-D42A-0259-FF10-D53274AB6000', 'v1');


   	$id = $_REQUEST['id'];
	  $idcategory = $_REQUEST['category'];

    if ($id == "") {
        header('LOCATION: brands.php');
    }else{

        $result = Backendless::$Persistence->of( "Book" )->findById( $id, $relation_depth= null ); 

        $user = Backendless::$Persistence->of( "UsersLogIn" )->findById( $_SESSION['userid'], $relation_depth= null );

        $book = new UserBook(); 

        $book->setBookId($result);
        $book->setUserId($user);

        Backendless::$Persistence->save( $book );  

        //$_SESSION['added'] = true;

        //header('LOCATION: brandbook2.php?id='. $idcategory);
    }
?>