<?php

   session_start();
    
   if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    
   } else {
      header('Location: index.php');   
    exit;
  }  
 
?>

<?php

    use backendless\Backendless;
    use backendless\services\persistence\BackendlessDataQuery;
    include "./autoload.php";

    Backendless::initApp('21880115-D6BD-1ECE-FF0D-234E6063AA00', '12BD10F1-D42A-0259-FF10-D53274AB6000', 'v1');


    $id = $_GET['id'];

    if ($id == "") {
        header('LOCATION: brands.php');
    }else{
        $data_query = new BackendlessDataQuery();
        $condition = "CategoryId.objectId='" . $id . "'";
        $data_query->setWhereClause( $condition );
        $result = Backendless::$Persistence->of( "Book" )->find( $data_query )->getAsClasses();        
    }
?>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>PDFShared - Agencia J.E. Handal</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

<link href="assets/plugins/morrisjs/morris.css" rel="stylesheet" />
<link href="assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
<link href="assets/css/main.css" rel="stylesheet">
<link href="assets/css/themes/all-themes.css" rel="stylesheet" />

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script type="text/javascript">
    function addbook(id, cate) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", "add.php?id=" + id + "&category=" + cate, true);
        xmlhttp.send();
        swal("Added Book!", "The Book Was Added To Your Book", "success");
    }
</script>

</head>

<body class="theme-blue ls-closed">
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>

<div class="overlay"></div>

<ul id="f-menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
  <li class="mfb-component__wrap">
    <a href="#" class="mfb-component__button--main g-bg-cyan">
      <i class="mfb-component__main-icon--resting zmdi zmdi-plus"></i>
      <i class="mfb-component__main-icon--active zmdi zmdi-close"></i>
    </a>
    <ul class="mfb-component__list">
      <li>
        <a href="mail-inbox.html" data-mfb-label="Inbox" class="mfb-component__button--child bg-blue">
          <i class="zmdi zmdi-email mfb-component__child-icon"></i>
        </a>
      </li>
      <li>
        <a href="chat.html" data-mfb-label="Chat App" class="mfb-component__button--child bg-orange">
          <i class="zmdi zmdi-comments mfb-component__child-icon"></i>
        </a>
      </li>

      <li>
        <a href="blog.html" data-mfb-label="Blogger" class="mfb-component__button--child bg-purple">
          <i class="zmdi zmdi-blogger mfb-component__child-icon"></i>
        </a>
      </li>
    </ul>
  </li>
</ul>

<ul id="f-menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
  <li class="mfb-component__wrap">
    <a href="#" class="mfb-component__button--main g-bg-cyan">
      <i class="mfb-component__main-icon--resting zmdi zmdi-plus"></i>
      <i class="mfb-component__main-icon--active zmdi zmdi-close"></i>
    </a>
    <ul class="mfb-component__list">
      <li>
        <a href="brands.php" data-mfb-label="Add Book" class="mfb-component__button--child bg-blue">
          <i class="zmdi zmdi-library mfb-component__child-icon"></i>
        </a>
      </li>
      <li>
        <a href="chat.html" data-mfb-label="Chat App" class="mfb-component__button--child bg-orange">
          <i class="zmdi zmdi-comments mfb-component__child-icon"></i>
        </a>
      </li>      
    </ul>
  </li>
</ul>

<div id="morphsearch" class="morphsearch">    
    <span class="morphsearch-close"></span> </div>
<nav class="navbar clearHeader">
    <div class="container-fluid">
        <div class="navbar-header"> <a href="javascript:void(0);" class="bars"></a> <a class="navbar-brand" >PDFShared - Agencia J.E. Handal</a> </div>

        <ul class="nav navbar-nav navbar-right">               
           
            <li><a href="logout.php" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-sign-in"></i></a></li>
        </ul>
    </div>
</nav>

<section> 
    
    <aside id="leftsidebar" class="sidebar"> 
       
       <div class="user-info">
            <div class="admin-image"> <img src="assets/images/random-avatar7.jpg" alt=""> </div>
            <div class="admin-action-info"> <span>Welcome</span>
                <h3><?php echo $_SESSION['username'] ?></h3>
                <ul>
                    <li><a data-placement="bottom" title="Go to Inbox" href="mail-inbox.html"><i class="zmdi zmdi-email"></i></a></li>
                    <li><a data-placement="bottom" title="Go to Profile" href="profile.html"><i class="zmdi zmdi-account"></i></a></li>
                    <li><a data-placement="bottom" title="Log out" href="logout.php" ><i class="zmdi zmdi-sign-in"></i></a></li>
                    <li><a href="#" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-settings"></i></a></li>                   
                </ul>
            </div>
            <div class="quick-stats">
                <h5>Today Report</h5>
                <ul>
                    <li><span><?php echo count($result); ?><i>PDF Files</i></span></li>
                    <li><span>85<i>Videos</i></span></li>
                </ul>
            </div>
        </div>


        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li  class="active open"><a href="menu.php"><i class="zmdi zmdi-library"></i><span>My Book List</span> </a> </li>                

                <li> <a href="#"><i class="zmdi zmdi-collection-video"></i><span>Video List</span> </a> </li>    
               
            </ul>
        </div>
        

        <div class="legal">
            <div class="copyright"> &copy; 2017 <a href="http://agenciajehandal.com/">Agencia J.E. Handal S.A. de C.V.</a>. </div>
            <div class="version"> <b>Version: </b> 1.0.0 </div>
        </div>
        <!-- #Footer --> 
    </aside>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2></h2>            
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">                        
                        <h2> All Books Of Brand: <?php echo $result[0]['CategoryId']['CategoryName'] ?> </h2>                                                
                    </div>
                    <div class="body">
                        

                        <div class="row clearfix js-sweetalert">  
                            <?php
                                for ($i=0; $i < count($result); $i++) { 
                                    $name = $result[$i]['bookName'];
                                    $category = $result[$i]['CategoryId']['CategoryName'];
                                    $url = $result[$i]['bookThumbnailUrl'];                           

                                    echo        '<div class="col-sm-6 col-md-3">' .
                                                    '<div class="thumbnail">' .
                                                        '<a href="pdf.php?id=' . $result[$i]['objectId'] .  '">' .
                                                        '<img src="' . $url .'" height="100" alt="" />' .
                                                        '<div class="caption">' . 
                                                            '<h3>' . $name .'</h3>' .                                                            
                                                        '</div> </a>' .
                                                        '<a class="btn btn-raised waves-effect g-bg-blue" onclick="' . 'addbook(' . "'" . $result[$i]['objectId'] . "'" . "," . "'" . $result[$i]['CategoryId']['objectId'] . "'" . ')' . '" >Add Book To My List</a>' .                                                                                                                                                                       
                                                    '</div>' .
                                                '</div>';                                                                                          
                                }
                            ?>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="color-bg"></div>
<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js --> 
<script src="assets/plugins/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 

<script src="assets/js/pages/ui/dialogs.js"></script> 

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 

<script src="assets/js/pages/index.js"></script>



</body>


</html>