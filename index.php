<!DOCTYPE html>
<html>


<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>PDFShared - Agencia J.E. Handal</title>

<link rel="icon" href="favicon.ico" type="image/x-icon">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link href="assets/css/main.css" rel="stylesheet">
<link href="assets/css/login.css" rel="stylesheet">


<link href="assets/css/themes/all-themes.css" rel="stylesheet" />
</head>
<body class="login-page authentication">

<div class="container">
    <div class="card-top"></div>
    <div class="card">
        <h1 class="title"><span>PDFShare - Agencia J.E. Handal</span>Login <div class="msg">Sign in to start your session</div></h1>
        <div class="col-md-12">
            <form id="sign_in" method="POST" action="checklogin.php">
                
                <div class="input-group"> <span class="input-group-addon"> <i class="zmdi zmdi-account"></i> </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="email" placeholder="Email" required autofocus>
                    </div>
                </div>
                <div class="input-group"> <span class="input-group-addon"> <i class="zmdi zmdi-lock"></i> </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                </div>
                <div>                    
                    <div class="text-center">
                        <input type="submit" class="btn btn-raised waves-effect g-bg-cyan" name = "btsubmit" value = "SIGN IN"> 
                        <a href="sign-up.php" hre class="btn btn-raised waves-effect" type="submit" name="sign-up">SIGN UP</a>
                    </div>
                    <div class="text-center"> <a href="forgot-password.html">Forgot Password?</a></div>
                </div>
            </form>
        </div>
    </div>    
</div>
<div class="theme-bg"></div>

<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> 


<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/js/pages/examples/sign-in.js"></script>

</body>

</html>