<?php

   session_start();
    
   if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
   } else {
      header('Location: index.php');   
    exit;
  }  
 
?>

<?php

use backendless\Backendless;
use backendless\services\persistence\BackendlessDataQuery;
include "./autoload.php";
include "./UserBook.php";

Backendless::initApp('21880115-D6BD-1ECE-FF0D-234E6063AA00', '12BD10F1-D42A-0259-FF10-D53274AB6000', 'v1');

$id = $_REQUEST['id'];

if ($id == "") {
	header('LOCATION: menu.php');
}else{

	$result = Backendless::$Persistence->of("UserBook")->findById( $id, $relation_depth= null );

  $userbook = new UserBook();

  $userbook->setObjectId($result['objectId']);

	Backendless::$Persistence->of('UserBook')->remove( $userbook );

	//header('LOCATION: menu.php');

}

?>